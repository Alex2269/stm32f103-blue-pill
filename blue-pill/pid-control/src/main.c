/****************************************************************************\
 File:          main.c
 Date:
 
 Description:
 
 Known bugs/missing features:
 
\****************************************************************************/

#include <stdint.h>
#include <pid.h>
#include "rcc_clock.h"
#include "stm32f1xx.h"

#define SYSCLOCK 72000000U

__IO uint32_t tmpreg;
__IO uint32_t SysTick_CNT = 0;

inline void delay(__IO uint32_t tck)
{
  while(tck)
  {
    tck--;
  }  
}

void delay_ms(uint32_t ms)
{
  SysTick->VAL |= SYSCLOCK / 1000 - 1;

  SysTick_CNT = ms;
  while(SysTick_CNT) {}
}

int main(void)
{
  init_clock();
   pid_output_typedef pid_output = {0};

  while (1) {
      if (1) {
        pid_compute(2, 5, 4, &pid_output.pitch); 
      }
  }
}

void SysTick_Handler(void)
{
   if(SysTick_CNT > 0)  SysTick_CNT--;
}
